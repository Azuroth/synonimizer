"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Synonymizer by Group C for novi.digital
University of Lancaster 2018

Code written by Zachariah Tan
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
----------------------------------LIBRARIES--------------------------------------
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

#nltk.download() #Uncomment and run this line to download the nltk library.
import nltk
import re
import requests
import time
from bs4 import BeautifulSoup
from nltk.tokenize import regexp_tokenize
from nltk.corpus import stopwords
from nltk.corpus import wordnet as wn


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
---------------------------------WEBSCRAPING-------------------------------------
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
#Takes specified information from a web page and loads it into a list.
def WebScrape(link):
    page = requests.get(link) #Request information.
    soup = BeautifulSoup(page.content, "html.parser") #Get all data from the html page.
    text_raw = soup.find_all('p') #Find all <p> paragraph tags and store it. Change ('p') to extract different portions.

    #Convert BS4 variable type to a list of strings.
    text_new = []
    for x in text_raw:
        text_new.append(str(x))
    return text_new


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
--------------------------------PREPROCESSING------------------------------------
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

#Takes a list of words and formats it into a single concatenated string.
def Preprocess(text):
    text_new = ""
    for word in text:
        word = re.sub(r'<.*?>', '', word) #Remove html tags.
        for x in word.split():
            #Concatenate into one string with only the specified character types.
            text_new = text_new + " " + "".join(regexp_tokenize(x, "[\w.!?]+"))
    return text_new


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
----------------------------------POSTAGGING-------------------------------------
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

#Taken from https://www.programcreek.com/python/example/91599/nltk.corpus.wordnet.ADJ
def wordnet_pos_code(tag):
    if tag.startswith('N'):
        return wn.NOUN
    elif tag.startswith('V'):
        return wn.VERB
    elif tag.startswith('J'):
        return wn.ADJ
    elif tag.startswith('R'):
        return wn.ADV
    else:
        return None

#Returns each word in a list as a pair of the word and it's type (ie. noun, verb, etc...)
def POSTag(text):
    POSTagged = nltk.pos_tag(text.split())
    return POSTagged


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
------------------------------SYNONYM EXTRACTION---------------------------------
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

#Like Preprocess but with slight differences.
def Format(text):
    text_new = "" 
    for word in text:
        for x in word.split():
            text_new = text_new + " " + "".join(regexp_tokenize(x, "[\w.!?|]+")) #Included |.
    return text_new

#Determines if words have synonyms and selects the most appropriate set of synonyms where required.
def Synonymize(POSTagged):
    start = time.time() #used to record computation time.
    engsw = stopwords.words('english') #Words to not find synonyms for.
    words = list()
    texts = [x[0] for x in POSTagged] #Extract the text portion of the POS tagged list.
    count = -1
    
    for text in texts:
        count += 1
        if engsw.__contains__(text.lower()):
             words.append(text)
             continue

        #For the current text determine the synsets with respect to the pos tag.
        lAllSynsets = list(wn.synsets(text, pos = wordnet_pos_code(POSTagged[count][1])))
        CountersForSynsets = [dict({'Syn' : lAllSynsets[j], 'count' : 0}) for j in range(len(lAllSynsets))]

        for j in range(len(lAllSynsets)):
            for k in range (-10, 10):
                if (count + k < 0 or count + k >= len(texts)):
                    continue
                if (engsw.__contains__(texts[count + k].lower())):
                    continue
                if (lAllSynsets[j].examples().__contains__(texts[count + k].lower())> 0):
                    CountersForSynsets[j]['count'] += 1
                elif(lAllSynsets[j].definition().lower().__contains__(texts[count + k].lower())):
                    CountersForSynsets[j]['count'] += 1
        SortedCounts = sorted(CountersForSynsets, key = lambda x: x['count'])

        if (len(SortedCounts) > 0): #Check if word has synonyms.
            e = SortedCounts[0]
            syn = e['Syn'].lemmas() #lemmas returns a list of synonyms for the given string.
            synonym = list()
            index = 0

            for lemma in syn: #Iterate through synonyms.
                if (index < 3): #Only give a set number of synonyms per word.
                    if (lemma.name()):
                        synonym.append(lemma.name())
                        syns = '|'.join(synonym) #Concatenate all synonyms into one element for storage.
                        index += 1
            words.append(syns) #Store the concatenated list of synonyms.
        else:
            words.append(text) #If no synonyms are found, just add the word to the list.

    words = Format(words) #Concatenate list into a formatted string of text.
    end = time.time()-start
    print ("The program took", end, "to run")
    return words


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
-------------------------------------MAIN----------------------------------------
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

text = WebScrape('https://www.theatlantic.com/magazine/archive/2001/05/the-hunters-wife/302198/') #Input the link you want to scrape here.
text = Preprocess(text)
#print(text)
POSTagged = POSTag(text)
result = Synonymize(POSTagged)
#print(POSTagged)
print(result)
